/**
 * Created by alberto on 17/05/15.
 */
public class Adder {
    public int add(String[] numbers) throws InvalidNumberException {

        int sum = 0;

        for(String number : numbers) {
            int value = Integer.parseInt(number);
            if(value < 0)
                throw new InvalidNumberException();
            sum += Integer.parseInt(number);
        }

        return sum;
    }

    public void initialize() {
        //
    }

    public void dispose() {
        //
    }
}
