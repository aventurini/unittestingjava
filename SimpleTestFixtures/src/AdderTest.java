import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class AdderTest {

    private Adder adder;

    @Before
    public void setup() {
        adder = new Adder();
        adder.initialize();
    }

    @After
    public void teardown() {
        adder.dispose();
    }

    @Test
    public void add_DueNumeri_RitornaLaSomma() throws Throwable {
        int result = adder.add(new String[] { "1", "2" });

        assertEquals(3, result);
    }

    @Test(expected = InvalidNumberException.class)
    public void add_NumeroNegativo_LanciaInvalidNumberException() throws Throwable {
        int result = adder.add(new String[] { "1", "-1" });
    }


    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void add_NumeroNegativo_LanciaInvalidNumberException2() throws Throwable {
        thrown.expect(InvalidNumberException.class);
        int result = adder.add(new String[] { "1", "-1" });
    }
}
