/**
 * Created by alberto on 17/05/15.
 */
public class Main {
    public static void main(String[] args) {
        try {
            add_DueNumeri_RitornaLaSomma();
            System.out.println("Il test ha avuto successo");
        } catch (Exception e) {
            System.out.println("Il test è fallito");
        }
    }

    public static void add_DueNumeri_RitornaLaSomma() throws Exception {
        Adder adder = new Adder();
        int result = adder.add(new String[] { "1", "2" });
        if(result != 3) throw new Exception("Test failed");
    }
}
