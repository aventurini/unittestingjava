/**
 * Created by alberto on 17/05/15.
 */
public class Adder {
    public int add(String[] numbers) {
        int sum = 0;
        for(String number : numbers)
           sum += Integer.parseInt(number);
        return sum;
    }
}
