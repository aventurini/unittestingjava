
/**
 * Created by alberto on 17/05/15.
 */
public class AdderTestNoJunit {

    public void add_DueNumeri_RitornaLaSomma() throws Exception {
        Adder adder = new Adder();
        int result = adder.add(new String[] { "1", "2" });
        if(result != 3) throw new Exception("Test failed");
    }
}
