import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class AdderTest {
    @Test
    public void add_DueNumeri_RitornaLaSomma() {
        Adder adder = new Adder();
        int result = adder.add(new String[] { "1", "2" });
        assertEquals(3, result);
    }
}
