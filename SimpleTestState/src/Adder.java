/**
 * Created by alberto on 17/05/15.
 */
public class Adder {

    private int total = 0;

    public int add(String[] numbers) {
        int sum = 0;

        for(String number : numbers)
           sum += Integer.parseInt(number);

        total += sum;

        return sum;
    }

    public int getTotal() {
        return total;
    }
}
