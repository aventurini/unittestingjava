import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class AdderTest {
    @Test
    public void add_ChiamataDueVolte_totalContieneLaSommaTotaleDelleAddizioni() {
        Adder adder = new Adder();

        adder.add(new String[] { "1", "2" });
        adder.add(new String[] { "3", "4" });

        assertEquals(10, adder.getTotal());
    }
}
