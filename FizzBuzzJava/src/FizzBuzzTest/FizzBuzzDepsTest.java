package FizzBuzzTest;

import FizzBuzz.Buzzer.Buzzer;
import FizzBuzz.FizzBuzzDeps;
import FizzBuzz.Fizzer.Fizzer;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by alberto on 17/05/15.
 */


class MockFizzer implements Fizzer {
    public boolean fizzWasCalled = false;

    public String fizz() {
        fizzWasCalled = true;
        return "";
    }
}

class MockBuzzer implements Buzzer {
    public boolean buzzWasCalled = false;

    public String buzz() {
        buzzWasCalled = true;
        return "";
    }
}

public class FizzBuzzDepsTest {

    @Test
    public void playNumber_whenFizz_callsFizzer() {
        MockFizzer fizzer = new MockFizzer();
        MockBuzzer buzzer = new MockBuzzer();
        FizzBuzzDeps fizzBuzz = new FizzBuzzDeps(fizzer, buzzer);

        String result = fizzBuzz.playNumber(3);

        assertTrue(fizzer.fizzWasCalled);
    }

    @Test
    public void playNumber_whenBuzz_callsBuzzer() {
        MockFizzer fizzer = new MockFizzer();
        MockBuzzer buzzer = new MockBuzzer();
        FizzBuzzDeps fizzBuzz = new FizzBuzzDeps(fizzer, buzzer);

        String result = fizzBuzz.playNumber(5);

        assertTrue(buzzer.buzzWasCalled);
    }

}
