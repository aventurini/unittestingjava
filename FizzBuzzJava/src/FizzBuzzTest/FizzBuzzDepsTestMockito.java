package FizzBuzzTest;

import FizzBuzz.Buzzer.Buzzer;
import FizzBuzz.FizzBuzzDeps;
import FizzBuzz.Fizzer.Fizzer;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;

/**
 * Created by alberto on 17/05/15.
 */
public class FizzBuzzDepsTestMockito {

    @Test
    public void playNumber_whenFizz_callsFizzer() {
        Fizzer fizzer = Mockito.mock(Fizzer.class);
        Buzzer buzzer = Mockito.mock(Buzzer.class);
        FizzBuzzDeps fizzBuzz = new FizzBuzzDeps(fizzer, buzzer);

        String result = fizzBuzz.playNumber(3);

        Mockito.verify(fizzer).fizz();
    }

    @Test
    public void playNumber_whenBuzz_callsBuzzer() {
        Fizzer fizzer = Mockito.mock(Fizzer.class);
        Buzzer buzzer = Mockito.mock(Buzzer.class);
        FizzBuzzDeps fizzBuzz = new FizzBuzzDeps(fizzer, buzzer);

        String result = fizzBuzz.playNumber(5);

        Mockito.verify(buzzer).buzz();
    }

}
