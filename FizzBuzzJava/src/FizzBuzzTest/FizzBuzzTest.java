package FizzBuzzTest;

import org.junit.Test;
import FizzBuzz.FizzBuzz;
import static org.junit.Assert.*;

/**
 * Created by alberto on 16/05/15.
 */
public class FizzBuzzTest {

    @Test
    public void playNumber_withOne_returnsTheNumber() {
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.playNumber(1);

        assertEquals("1", result);
    }

    @Test
    public void playNumber_withThree_returnsFizz() {
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.playNumber(3);

        assertEquals("fizz", result);
    }

    @Test
    public void playNumber_withFive_returnsBuzz() {
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.playNumber(5);

        assertEquals("buzz", result);
    }

    @Test
    public void playNumber_withFifteen_returnsFizzBuzz() {
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.playNumber(15);

        assertEquals("fizzbuzz", result);
    }
}
