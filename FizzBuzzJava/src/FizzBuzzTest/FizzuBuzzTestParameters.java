package FizzBuzzTest;

import FizzBuzz.FizzBuzz;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by alberto on 17/05/15.
 */

@RunWith(Parameterized.class)
public class FizzuBuzzTestParameters {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {1, "1"}, {2, "2"}, {3, "fizz"}, {4, "4"}, {5, "buzz"}, {15, "fizzbuzz"}
        });
    }

    private int input;
    private String expected;

    public FizzuBuzzTestParameters(int input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void playNumber_withAnyNumber_returnsTheCorrectNumber() {
        FizzBuzz fizzBuzz = new FizzBuzz();

        String result = fizzBuzz.playNumber(input);

        assertEquals(expected, result);
    }
}
