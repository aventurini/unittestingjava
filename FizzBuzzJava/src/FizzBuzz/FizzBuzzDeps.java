package FizzBuzz;

import FizzBuzz.Buzzer.Buzzer;
import FizzBuzz.Fizzer.Fizzer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alberto on 17/05/15.
 */
public class FizzBuzzDeps {
    private Fizzer fizzer;
    private Buzzer buzzer;

    public FizzBuzzDeps(Fizzer fizzer, Buzzer buzzer) {
        this.fizzer = fizzer;
        this.buzzer = buzzer;
    }

    public String[] play(int[] numbers) {
        List<String> result = new ArrayList<String>();

        for (Integer number : numbers) {
            result.add(playNumber(number));
        }

        return result.toArray(new String[result.size()]);
    }

    public String playNumber(int number) {
        StringBuilder result = new StringBuilder();

        handleFizz(number, result);
        handleBuzz(number, result);

        if(result.length() != 0)
            return result.toString();

        return Integer.toString(number);
    }

    private void handleFizz(int number, StringBuilder result) {
        if(number % 3 == 0) {
            result.append(fizzer.fizz());
        }
    }

    private void handleBuzz(int number, StringBuilder result) {
        if(number % 5 == 0) {
            result.append(buzzer.buzz());
        }
    }
}
