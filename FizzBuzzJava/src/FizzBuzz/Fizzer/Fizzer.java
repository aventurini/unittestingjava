package FizzBuzz.Fizzer;

/**
 * Created by alberto on 17/05/15.
 */
public interface Fizzer {
    public String fizz();
}
