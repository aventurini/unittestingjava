package FizzBuzz.Buzzer;

/**
 * Created by alberto on 17/05/15.
 */
public interface Buzzer {
    public String buzz();
}
