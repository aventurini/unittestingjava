package FizzBuzz;

import java.util.HashMap;

/**
 * Created by alberto on 16/05/15.
 */
public class FizzBuzzState {

    public FizzBuzzState() {
    }

    public String play(int number) {

        String result = "";

        if(number % 3 == 0)
            handleFizz(result);

        if(number % 5 == 0)
            handleBuzz(result);

        if(result.isEmpty())
            return Integer.toString(number);

        return result;
    }

    private void handleFizz(String result) {

    }

    private void handleBuzz(String result) {
        
    }
}
