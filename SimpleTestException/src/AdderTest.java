import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class AdderTest {

    @Test
    public void add_DueNumeri_RitornaLaSomma() throws Throwable {
        Adder adder = new Adder();

        int result = adder.add(new String[] { "1", "2" });

        assertEquals(3, result);
    }

    @Test(expected = InvalidNumberException.class)
    public void add_NumeroNegativo_LanciaInvalidNumberException() throws Throwable {
        Adder adder = new Adder();

        int result = adder.add(new String[] { "1", "-1" });
    }


    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void add_NumeroNegativo_LanciaInvalidNumberException2() throws Throwable {
        Adder adder = new Adder();

        thrown.expect(InvalidNumberException.class);
        int result = adder.add(new String[] { "1", "-1" });
    }
}
