package PolishCalculator.Expressions;

/**
 * Created by alberto on 15/05/15.
 */
public interface Expression {
    public Integer evaluate();
}
