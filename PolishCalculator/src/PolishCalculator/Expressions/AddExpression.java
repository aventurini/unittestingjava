package PolishCalculator.Expressions;

/**
 * Created by alberto on 15/05/15.
 */
public class AddExpression extends BinaryExpression {

    public AddExpression(Expression first, Expression second) {
        super(first, second);
    }

    @Override
    public Integer evaluate() {
        return first.evaluate() + second.evaluate();
    }
}
