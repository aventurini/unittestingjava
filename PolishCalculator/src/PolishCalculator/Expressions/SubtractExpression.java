package PolishCalculator.Expressions;

/**
 * Created by alberto on 15/05/15.
 */
public class SubtractExpression extends BinaryExpression {

    public SubtractExpression(Expression first, Expression second) {
        super(first, second);
    }

    @Override
    public Integer evaluate() {
        return first.evaluate() - second.evaluate();
    }
}
