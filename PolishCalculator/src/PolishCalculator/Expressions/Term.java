package PolishCalculator.Expressions;

/**
 * Created by alberto on 15/05/15.
 */
public class Term implements Expression {

    private Integer value;

    public Term(Integer value) {
        this.value = value;
    }

    @Override
    public Integer evaluate() {
        return value;
    }
}
