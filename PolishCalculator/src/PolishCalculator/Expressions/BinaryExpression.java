package PolishCalculator.Expressions;

/**
 * Created by alberto on 15/05/15.
 */
public abstract class BinaryExpression implements Expression {
    protected Expression first;
    protected Expression second;

    public BinaryExpression(Expression first, Expression second) {
        this.first = first;
        this.second = second;
    }

}
