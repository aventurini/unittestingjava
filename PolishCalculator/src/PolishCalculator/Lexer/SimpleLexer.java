package PolishCalculator.Lexer;

import java.util.LinkedList;

/**
 * Created by alberto on 15/05/15.
 */
public class SimpleLexer implements Lexer {
    public LinkedList<String> tokenize(String input) {

        String[] tokens = input.split("\\s");

        LinkedList<String> tokenList = new LinkedList<String>();

        for(String token : tokens) {
            tokenList.add(token);
        }

        return tokenList;
    }
}
