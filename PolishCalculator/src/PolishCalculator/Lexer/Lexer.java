package PolishCalculator.Lexer;

import java.util.LinkedList;

/**
 * Created by alberto on 15/05/15.
 */
public interface Lexer {
    public LinkedList<String> tokenize(String input);
}
