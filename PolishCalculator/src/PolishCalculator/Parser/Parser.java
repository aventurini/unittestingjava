package PolishCalculator.Parser;

import PolishCalculator.Exceptions.ParseException;
import PolishCalculator.Expressions.Expression;

import java.util.LinkedList;

/**
 * Created by alberto on 15/05/15.
 */
public interface Parser {
    public Expression Parse(LinkedList<String> tokens) throws ParseException;
}
