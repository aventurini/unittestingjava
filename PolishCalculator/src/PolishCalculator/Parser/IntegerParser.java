package PolishCalculator.Parser;

import PolishCalculator.Exceptions.ParseException;
import PolishCalculator.Expressions.AddExpression;
import PolishCalculator.Expressions.Expression;
import PolishCalculator.Expressions.SubtractExpression;
import PolishCalculator.Expressions.Term;
import java.util.LinkedList;

/**
 * Created by alberto on 15/05/15.
 */
public class IntegerParser implements Parser {

    public Expression Parse(LinkedList<String> tokens) throws ParseException {
        if(tokens.size() == 0)
            throw new ParseException("Invalid number of tokens");

        String first = tokens.removeFirst();

        if(isNumber(first))
            return new Term(Integer.parseInt(first));

        Expression child1 = Parse(tokens);
        Expression child2 = Parse(tokens);

        if(first.equals("+"))
            return new AddExpression(child1, child2);

        if(first.equals("-"))
            return new SubtractExpression(child1, child2);

        throw new ParseException("Invalid token: " + first);
    }

    private boolean isNumber(String token) {
        return token.matches("\\d+");
    }

}
