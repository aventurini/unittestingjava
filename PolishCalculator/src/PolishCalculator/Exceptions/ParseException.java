package PolishCalculator.Exceptions;

/**
 * Created by alberto on 15/05/15.
 */
public class ParseException extends Exception {
    public ParseException(String message) {
        super(message);
    }
}
