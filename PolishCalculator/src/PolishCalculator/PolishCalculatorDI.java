package PolishCalculator;

import PolishCalculator.Exceptions.ParseException;
import PolishCalculator.Expressions.Expression;
import PolishCalculator.Lexer.Lexer;
import PolishCalculator.Parser.Parser;

import java.util.LinkedList;

/**
 * Created by alberto on 15/05/15.
 */
public class PolishCalculatorDI {

    private Lexer lexer;
    private Parser parser;

    public PolishCalculatorDI(Lexer lexer, Parser parser) {
        this.lexer = lexer;
        this.parser = parser;
    }

    public int evaluate(String input) throws ParseException {
        LinkedList<String> tokens = lexer.tokenize(input);
        Expression expression = parser.Parse(tokens);
        return expression.evaluate();
    }

}
