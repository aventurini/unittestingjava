package PolishCalculator;

import PolishCalculator.Exceptions.ParseException;
import PolishCalculator.Expressions.AddExpression;
import PolishCalculator.Expressions.Expression;
import PolishCalculator.Expressions.SubtractExpression;
import PolishCalculator.Expressions.Term;

import java.util.LinkedList;

/**
 * Created by alberto on 16/05/15.
 */
public class PolishCalculator {


    public int evaluate(String input) throws ParseException {
        LinkedList<String> tokens = tokenize(input);
        Expression expression = parse(tokens);
        return expression.evaluate();
    }

    private LinkedList<String> tokenize(String input) {
        String[] tokens = input.split("\\s");
        LinkedList<String> tokenList = new LinkedList<String>();

        for(String token : tokens) {
            tokenList.add(token);
        }

        return tokenList;
    }

    private Expression parse(LinkedList<String> tokens) throws ParseException {
        if(tokens.size() == 0)
            throw new ParseException("Invalid number of tokens");

        String first = tokens.removeFirst();

        if(isNumber(first))
            return new Term(Integer.parseInt(first));

        Expression child1 = parse(tokens);
        Expression child2 = parse(tokens);

        if(first.equals("+"))
            return new AddExpression(child1, child2);

        if(first.equals("-"))
            return new SubtractExpression(child1, child2);

        throw new ParseException("Invalid token: " + first);
    }

    private boolean isNumber(String token) {
        return token.matches("\\d+");
    }
}
