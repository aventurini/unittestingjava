import PolishCalculator.Exceptions.ParseException;
import PolishCalculator.Lexer.Lexer;
import PolishCalculator.Lexer.SimpleLexer;
import PolishCalculator.Parser.IntegerParser;
import PolishCalculator.Parser.Parser;
import PolishCalculator.PolishCalculator;
import PolishCalculator.PolishCalculatorDI;

/**
 * Created by alberto on 15/05/15.
 */
public class Main {
    public static void main(String[] args) {

        PolishCalculator calculator = new PolishCalculator();

        try {
            int result = calculator.evaluate("+ 1 3");
            System.out.println(result);
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
