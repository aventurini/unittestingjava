package PolishCalculatorTest;

import PolishCalculator.Exceptions.ParseException;
import PolishCalculator.PolishCalculator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

/**
 * Created by alberto on 16/05/15.
 */
public class PolishCalculatorTest {

    @Test
    public void evaluate_oneNumber_returnsThatNumber() throws Throwable {
        PolishCalculator calc = new PolishCalculator();

        int result = calc.evaluate("1");

        assertEquals(1, result);
    }

//    @Test(expected = ParseException.class)
//    public void evaluate_noNumbers_throwsException() throws ParseException {
//        PolishCalculator calc = new PolishCalculator();
//
//        int result = calc.evaluate("");
//    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void evaluate_noNumbers_throwsException() throws Throwable {
        PolishCalculator calc = new PolishCalculator();

        thrown.expect(ParseException.class);
        int result = calc.evaluate("");
    }

    @Test
    public void evaluate_additionWithTwoNumbers_returnsAddedNumbers() throws Throwable {
        PolishCalculator calc = new PolishCalculator();

        int result = calc.evaluate("+ 1 2");

        assertEquals(3, result);
    }
}
