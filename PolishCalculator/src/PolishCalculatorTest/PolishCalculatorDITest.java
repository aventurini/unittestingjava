package PolishCalculatorTest;

import PolishCalculator.Exceptions.ParseException;
import PolishCalculator.Lexer.Lexer;
import PolishCalculator.Lexer.SimpleLexer;
import PolishCalculator.Parser.IntegerParser;
import PolishCalculator.Parser.Parser;
import PolishCalculator.PolishCalculatorDI;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by alberto on 16/05/15.
 */
public class PolishCalculatorDITest {

    @Test
    public void evaluate_noNumbers_throwsException() throws ParseException {
        Parser parser = new IntegerParser();
        Lexer lexer = new SimpleLexer();
        PolishCalculatorDI calc = new PolishCalculatorDI(lexer, parser);
// TODO
    }

    @Test
    public void evaluate_oneNumber_returnsThatNumber() throws ParseException {
        Parser parser = new IntegerParser();
        Lexer lexer = new SimpleLexer();
        PolishCalculatorDI calc = new PolishCalculatorDI(lexer, parser);

        int result = calc.evaluate("1");

        assertEquals(1, result);
    }
}
