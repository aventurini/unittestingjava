import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AdderTest {

    @Test
    public void add_DueNumeri_RitornaLaSomma() {
        Adder adder = new Adder();
        int result = adder.add(new String[] { "1", "2" });
        assertEquals(3, result);
    }
}
